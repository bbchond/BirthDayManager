package birthday.syj.com.manage.network;

import birthday.syj.com.manage.entity.ResponseHead;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by syj on 2017/12/18.
 */

public interface BirthService {
    @FormUrlEncoded
    @POST("fortune?appkey=af55cee0eb1219d8")
    Observable<ResponseHead> getAstro(@Field("astroid") int astroid, @Field("date") String date);
}
