package birthday.syj.com.manage.entity;

import birthday.syj.com.manage.util.JsonToString;

/**
 * Created by syj on 2017/12/18.
 */

public class ResultToday extends JsonToString {

    private String date;
    private String presummary;
    private String star;
    private String color;
    private String number;
    private String summary;
    private String money;
    private String career;
    private String love;
    private String health;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPresummary() {
        return presummary;
    }

    public void setPresummary(String presummary) {
        this.presummary = presummary;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getLove() {
        return love;
    }

    public void setLove(String love) {
        this.love = love;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }
}
