package birthday.syj.com.manage.common.showStarInfo;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import birthday.syj.com.manage.R;

public class ShowTomorrowInfo extends AppCompatActivity {

    TextView tomorrowDate;
    TextView tomorrowPresummary;
    TextView tomorrowSummary;
    TextView tomorrowLove;
    TextView tomorrowMoney;
    TextView tomorrowCareer;
    TextView tomorrowStar;
    TextView tomorrowColor;
    TextView tomorrowNumber;
    TextView tomorrowHealth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_tomorrow_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //添加toolbar支持
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initViews();
        Bundle bundle = getIntent().getExtras();
        tomorrowDate.setText(bundle.getString("getTomorrowDate"));
        tomorrowPresummary.setText(bundle.getString("getTomorrowPresummary"));
        tomorrowSummary.setText(bundle.getString("getTomorrowSummary"));
        tomorrowLove.setText(bundle.getString("getTomorrowLove"));
        tomorrowMoney.setText(bundle.getString("getTomorrowMoney"));
        tomorrowCareer.setText(bundle.getString("getTomorrowCareer"));
        tomorrowStar.setText(bundle.getString("getTomorrowStar"));
        tomorrowColor.setText(bundle.getString("getTomorrowColor"));
        tomorrowNumber.setText(bundle.getString("getTomorrowNumber"));
        tomorrowHealth.setText(bundle.getString("getTomorrowHealth"));
    }

    private void initViews() {
        tomorrowDate = (TextView) findViewById(R.id.date);
        tomorrowPresummary = (TextView) findViewById(R.id.presummary);
        tomorrowSummary = (TextView) findViewById(R.id.summary);
        tomorrowLove = (TextView) findViewById(R.id.love);
        tomorrowMoney = (TextView) findViewById(R.id.money);
        tomorrowCareer = (TextView) findViewById(R.id.career);
        tomorrowStar = (TextView) findViewById(R.id.star);
        tomorrowColor = (TextView) findViewById(R.id.color);
        tomorrowNumber = (TextView) findViewById(R.id.number);
        tomorrowHealth = (TextView) findViewById(R.id.health);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //设置返回标题栏返回键点击事件
            case android.R.id.home:
                finish();
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
