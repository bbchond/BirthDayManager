package birthday.syj.com.manage.common.showStarInfo;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import birthday.syj.com.manage.R;

public class ShowStarFuture extends AppCompatActivity {

    TextView getAstro;
    TextView date;
    TextView presummary;
    TextView summary;
    TextView love;
    TextView money;
    TextView career;
    TextView star;
    TextView color;
    TextView number;
    TextView health;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_star_future);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        //添加toolbar支持
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initViews();
        Bundle bundle = getIntent().getExtras();
        getAstro.setText(bundle.getString("getAstroname"));
        date.setText(bundle.getString("getDate"));
        presummary.setText(bundle.getString("getPresummary"));
        summary.setText(bundle.getString("getSummary"));
        love.setText(bundle.getString("getLove"));
        money.setText(bundle.getString("getMoney"));
        career.setText(bundle.getString("getCareer"));
        star.setText(bundle.getString("getStar"));
        color.setText(bundle.getString("getColor"));
        number.setText(bundle.getString("getNumber"));
        health.setText(bundle.getString("getHealth"));
    }

    private void initViews() {
        getAstro = (TextView) findViewById(R.id.getAstro);
        date = (TextView) findViewById(R.id.date);
        presummary = (TextView) findViewById(R.id.presummary);
        summary = (TextView) findViewById(R.id.summary);
        love = (TextView) findViewById(R.id.love);
        money = (TextView) findViewById(R.id.money);
        career = (TextView) findViewById(R.id.career);
        star = (TextView) findViewById(R.id.star);
        color = (TextView) findViewById(R.id.color);
        number = (TextView) findViewById(R.id.number);
        health = (TextView) findViewById(R.id.health);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //设置返回标题栏返回键点击事件
            case android.R.id.home:
                finish();
                default:
                    break;
        }
        return super.onOptionsItemSelected(item);
    }
}
