package birthday.syj.com.manage.network;

import android.os.Build;
import android.webkit.WebSettings;

import birthday.syj.com.manage.BirthReminderApplication;

/**
 * Created by syj on 2017/12/8.
 */

public class ApiConstants {

    public static final String COMMON_UA_STR = getUserAgent();

    public static final String BASEURL = "http://api.jisuapi.com/astro/";

    private static String getUserAgent() {
        String userAgent = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                userAgent = WebSettings.getDefaultUserAgent(BirthReminderApplication.getInstance());
            } catch (Exception e) {
                userAgent = System.getProperty("http.agent");
            }
        } else {
            userAgent = System.getProperty("http.agent");
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0, length = userAgent.length(); i < length; i++) {
            char c = userAgent.charAt(i);
            if (c <= '\u001f' || c >= '\u007f') {
                sb.append(String.format("\\u%04x", (int) c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
