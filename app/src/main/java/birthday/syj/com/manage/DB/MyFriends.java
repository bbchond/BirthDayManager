package birthday.syj.com.manage.DB;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * Created by Dawangba on 2017/12/18.
 */

public class MyFriends extends DataSupport implements Serializable {

    private int id;
    private String name;
    private String sex;
    private String date;
    private String telephone;
    private String relation;
    private String remarks;
    private String remindDate;
    private String remindType;
    private int birthYear;
    private int birthMonth;
    private int birthDay;

    public MyFriends() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

//    public String getRemarks() {
//        return remarks;
//    }

//    public void setRemarks(String remarks) {
//        this.remarks = remarks;
//    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getRemindDate() {
        return remindDate;
    }

    public void setRemindDate(String remindDate) {
        this.remindDate = remindDate;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }

    public String getRemindType() {
        return remindType;
    }

    public void setRemindType(String remindType) {
        this.remindType = remindType;
    }
}
