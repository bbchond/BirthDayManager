package birthday.syj.com.manage.util;

import com.google.gson.Gson;

/**
 * Created by syj on 2017/12/8.
 */

public class JsonToString {

    private static Gson _gson=new Gson();
    @Override
    public String toString() {
        return _gson.toJson(this);
    }

}
