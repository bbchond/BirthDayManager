package birthday.syj.com.manage.entity;

import birthday.syj.com.manage.util.JsonToString;

/**
 * Created by syj on 2017/12/18.
 */

public class ResultWeek extends JsonToString {

    private String date;
    private String money;
    private String career;
    private String love;
    private String health;
    private String job;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getLove() {
        return love;
    }

    public void setLove(String love) {
        this.love = love;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
