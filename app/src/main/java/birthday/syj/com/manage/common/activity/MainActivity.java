package birthday.syj.com.manage.common.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.yanzhenjie.permission.AndPermission;

import org.litepal.crud.DataSupport;

import java.util.List;

import birthday.syj.com.manage.DB.MyFriends;
import birthday.syj.com.manage.R;
import birthday.syj.com.manage.adapter.FriendsAdapter;
import birthday.syj.com.manage.util.RecyclerItemClickListener;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    MyFriends myFriends;
    private RecyclerView recyclerView;
    private FriendsAdapter adapter;
    private List<MyFriends> myFriendsList;

    public final static int NAME = 1;

    int longClcikId;
    int getLongPosition;

    @Override
    protected void onResume() {
        super.onResume();
        //去掉之前的数据
        myFriendsList.clear();
        List<MyFriends> newList = DataSupport.findAll(MyFriends.class);
        myFriendsList.addAll(newList);
        adapter.notifyDataSetChanged();
        Log.e("litepalTest", "onResume");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Android M版本以上动态获取权限(此处获取了自动拨打电话以及读取联系人的权限)，使用了AndPermission库
        AndPermission.with(this)
                .requestCode(101)
                .permission(Manifest.permission.CALL_PHONE,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.VIBRATE)
                .send();

        myFriendsList = DataSupport.findAll(MyFriends.class);
        adapter = new FriendsAdapter(myFriendsList);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                int id = myFriendsList.get(position).getId();
                Log.e("id", String.valueOf(id));
                MyFriends myFriends = myFriendsList.get(position);
                Intent intent = new Intent(MainActivity.this, FriendsInfo.class);
                intent.putExtra("Content", myFriends);
                intent.putExtra("id",id);
                intent.putExtra("ActivityName", NAME);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                Log.d("MainActivity", "onItemLongClick: ");
                longClcikId = myFriendsList.get(position).getId();
                getLongPosition = position;
                view.showContextMenu();
            }
        }));
        //为recyclerView添加间隔线
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //设置toolbar
        setSupportActionBar(toolbar);
        registerForContextMenu(recyclerView);
    }

    private void deleteData(int id) {
        DataSupport.deleteAll(MyFriends.class, "id = ?", String.valueOf(id));
        Log.d("DataDelete", "deleteData: " + id);
    }

    //toolbar的选择菜单
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle("生日管家");
                dialog.setMessage("作者姓名："+"单玉洁"+
                        "\n" +"学号："+"Z09315203"+
                        "\n"+"程序功能简介："+"生日提醒"+
                        "\n"+"程序版本："+"V1.0");
                dialog.setPositiveButton("确定",null);
                dialog.show();
                break;
            case R.id.quit:
                finish();
                break;
            case R.id.commitUs:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:18020226866"));
                startActivity(intent);
                break;
            case R.id.action_add:
                showBottomSheetDialog();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showBottomSheetDialog() {
        final BottomSheetDialog sheetDialog = new BottomSheetDialog(this);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.activity_bottom_sheet, null);
        Button birthday = (Button) dialogView.findViewById(R.id.bottom_btn1);
        Button cancel = (Button) dialogView.findViewById(R.id.bottom_btn_cancel);

        //添加生日按钮 下方的生日按钮
        // TODO: 2017/12/22
        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddBirthDay.class);
                Bundle bundle = new Bundle();
                bundle.putInt("ActivityName", NAME);
                intent.putExtras(bundle);
                startActivity(intent);
                sheetDialog.dismiss();
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetDialog.dismiss();
            }
        });

        sheetDialog.setContentView(dialogView);
        sheetDialog.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.delete_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                deleteData(longClcikId);
                myFriendsList.remove(getLongPosition);
                adapter.notifyItemRemoved(getLongPosition);
                adapter.notifyItemRangeChanged(0, myFriendsList.size());
        }
        return super.onContextItemSelected(item);
    }
}
