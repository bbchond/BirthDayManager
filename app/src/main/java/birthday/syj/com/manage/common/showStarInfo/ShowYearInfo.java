package birthday.syj.com.manage.common.showStarInfo;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import birthday.syj.com.manage.R;

public class ShowYearInfo extends AppCompatActivity {

    TextView yearDate;
    TextView yearLove;
    TextView yearMoney;
    TextView yearCareer;
    TextView yearSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_year_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //添加toolbar支持
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initViews();
        Bundle bundle = getIntent().getExtras();
        yearDate.setText(bundle.getString("getYearDate"));
        yearLove.setText(bundle.getString("getYearLove"));
        yearMoney.setText(bundle.getString("getYearMoney"));
        yearCareer.setText(bundle.getString("getYearCareer"));
        yearSummary.setText(bundle.getString("getYearSummary"));
    }

    private void initViews() {
        yearDate = (TextView) findViewById(R.id.date);
        yearLove = (TextView) findViewById(R.id.love);
        yearMoney = (TextView) findViewById(R.id.money);
        yearCareer = (TextView) findViewById(R.id.career);
        yearSummary = (TextView) findViewById(R.id.summary);
    }
}
