package birthday.syj.com.manage.common.showStarInfo;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import birthday.syj.com.manage.R;

public class ShowMonthInfo extends AppCompatActivity {

    TextView monthDate;
    TextView monthLove;
    TextView monthMoney;
    TextView monthCareer;
    TextView monthSummary;
    TextView monthHealth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_month_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //添加toolbar支持
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initViews();
        Bundle bundle = getIntent().getExtras();
        monthDate.setText(bundle.getString("getMonthDate"));
        monthLove.setText(bundle.getString("getMonthLove"));
        monthMoney.setText(bundle.getString("getMonthMoney"));
        monthCareer.setText(bundle.getString("getMonthCareer"));
        monthSummary.setText(bundle.getString("getMonthSummary"));
        monthHealth.setText(bundle.getString("getMonthHealth"));
    }

    private void initViews() {
        monthDate = (TextView) findViewById(R.id.date);
        monthLove = (TextView) findViewById(R.id.love);
        monthMoney = (TextView) findViewById(R.id.money);
        monthCareer = (TextView) findViewById(R.id.career);
        monthSummary = (TextView) findViewById(R.id.summary);
        monthHealth = (TextView) findViewById(R.id.health);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //设置返回标题栏返回键点击事件
            case android.R.id.home:
                finish();
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
