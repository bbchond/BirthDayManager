package birthday.syj.com.manage.common.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.annotation.IdRes;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import birthday.syj.com.manage.DB.MyFriends;
import birthday.syj.com.manage.R;

public class AddBirthDay extends AppCompatActivity {

    private int getActivityName;
    ArrayAdapter<String> arrayAdapter;
    final int DATE_DIALOG = 1;
    int mYear,mMonth,mDay;
    String default_sex = "女";
    Calendar calendar = Calendar.getInstance();

    EditText autoTvName;
    EditText autoTvPhone;
//    EditText edtSummary;
    TextView tvBirthday;
    TextView tvRemindDate;
    TextView tvRemindType;
    AutoCompleteTextView autoTvRelationShip;
    RadioGroup sexRadio;
    RadioButton rdMale;
    RadioButton rdFemale;
    Button save;

    String showSex = "女";
    String remindDate;
    int getId;
    MyFriends myFriends = new MyFriends();
    MyListener listensex = new MyListener();

    private class MyListener implements CompoundButton.OnCheckedChangeListener,RadioGroup.OnCheckedChangeListener{

        /**
         * Called when the checked state of a compound button has changed.
         *
         * @param buttonView The compound button view whose state has changed.
         * @param isChecked  The new checked state of buttonView.
         */
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            if (isChecked){
//                Log.e("7777777777775555555", showSex);
//            }else {
//                showSex = default_sex;
//                Log.e("777777779999999", showSex);
//            }
//            myFriends.setSex(showSex);
        }

        /**
         * <p>Called when the checked radio button has changed. When the
         * selection is cleared, checkedId is -1.</p>
         *
         * @param group     the group in which the checked radio button has changed
         * @param checkedId the unique identifier of the newly checked radio button
         */
        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            if (checkedId == rdMale.getId()) {
                // 获得选中的值
                showSex = rdMale.getText().toString();
                Log.e("44444444444444444444", rdMale.getText().toString());
            }else {
                showSex = default_sex;
                Log.e("选择了女", default_sex);
            }
        }
    }



    // 跳转至添加页面 activity_add_birth_day.xml
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_birth_day);
        Log.e("展示添加页面", "添加内容");
        initViews();

        Bundle bundle = getIntent().getExtras();
        getActivityName = bundle.getInt("ActivityName", 0);
        Log.e("获得值getName", String.valueOf(getActivityName));
        getId = bundle.getInt("getId", 0);
        Log.e("获得ID", String.valueOf(getId));
        if (getActivityName == FriendsInfo.NAME) {
            Log.e("就是这个", "值相同");
            myFriends = DataSupport.find(MyFriends.class, getId);
            autoTvName.setText(myFriends.getName());
            autoTvPhone.setText(myFriends.getTelephone());
//            edtSummary.setText(myFriends.getRemarks());
            tvBirthday.setText(myFriends.getDate());
            tvRemindDate.setText(myFriends.getRemindDate());
            tvRemindType.setText(myFriends.getRemindType());

            // TODO: 2017/12/23
            if (myFriends.getSex().equals("男")){
                rdMale.setChecked(true);
//                rdFemale.setChecked(false);
                Log.e("选择男", "就是这样");
            }else {
                rdFemale.setChecked(true);
//                rdMale.setChecked(false);
                Log.e("选择女", "嗯，就这样");
            }
            autoTvRelationShip.setText(myFriends.getRelation());

        }





        //// TODO: 2017/12/22 add by syj
        // 检查信息是否输入完整
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(autoTvName.getText().toString()) ||
                        TextUtils.isEmpty(autoTvPhone.getText().toString()) ||
                        TextUtils.isEmpty(autoTvRelationShip.getText().toString()) ||
//                        TextUtils.isEmpty(edtSummary.getText().toString())||
                        TextUtils.isEmpty(tvBirthday.getText().toString())||
                        TextUtils.isEmpty(tvRemindDate.getText().toString())||
                        TextUtils.isEmpty(tvRemindType.getText().toString())) {
                    Toast.makeText(AddBirthDay.this, "请检查信息是否完整输入", Toast.LENGTH_SHORT).show();
                } else {
                    getNowString();
                    myFriends.save();
                    Log.e("结束了", "下一次");
                    finish();
                }
            }
        });
    }

    private void initViews() {
        autoTvName = (EditText) findViewById(R.id.autoTvName);
        autoTvPhone = (EditText) findViewById(R.id.autoTvPhone);
//        edtSummary = (EditText) findViewById(R.id.edtSummary);
        tvBirthday = (TextView) findViewById(R.id.tvBirthday);
        tvRemindDate = (TextView) findViewById(R.id.tvRemindDate);
        tvRemindType = (TextView) findViewById(R.id.tvRemindType);
        autoTvRelationShip = (AutoCompleteTextView) findViewById(R.id.autoTvRelationShip);
        sexRadio = (RadioGroup) findViewById(R.id.sexRadio);

        // 男女
        rdMale = (RadioButton) findViewById(R.id.rdMale);
        rdFemale = (RadioButton) findViewById(R.id.rdFemale);

//
        save = (Button) findViewById(R.id.save);
        registerForContextMenu(tvRemindType);

        //自动补全的关系TextView
        String[] arr = {"朋友","同学","死党","闺蜜","家人","客户","同事","老师","领导","长辈","晚辈","爸爸","妈妈","爷爷","奶奶"};
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arr);
        autoTvRelationShip.setAdapter(arrayAdapter);
        autoTvRelationShip.setThreshold(0);

        tvRemindType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.showContextMenu();
            }
        });
        // 监听性别的选择
        sexRadio.setOnCheckedChangeListener(listensex);

        tvBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                showDialog(DATE_DIALOG);
            }
        });

        tvRemindDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetDialog();
            }
        });
    }

    /**
     * 设置日期，利用StringBuffer追加
     */
    public void display(){
        tvBirthday.setText(new StringBuffer().append(mYear).append("年").append(mMonth+1).append("月").append(mDay).append("日"));
        myFriends.setDate(tvBirthday.getText().toString());
        myFriends.setBirthYear(mYear);
        myFriends.setBirthMonth(mMonth+1);
        myFriends.setBirthDay(mDay);
        Log.e("Birth", mYear+"/"+(mMonth+1)+"/"+mDay);
    }

    private DatePickerDialog.OnDateSetListener mDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mYear = year;
            mMonth = month;
            mDay = dayOfMonth;
            Log.e("时间选额", "选择");
            display();
            calendar.set(mYear, mMonth, mDay);
        }
    };

    /**
     * 弹框显示日期选择器
     * @param id
     * @return
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id){
            case DATE_DIALOG:
                Log.e("出阿森纳和i","asdasdasdasdadada");
                return new DatePickerDialog(this,mDateListener,mYear,mMonth,mDay);
        }
        return null;
    }

    /**
     * 插入数据,保存数据
     */
    private void getNowString() {
        myFriends.setName(autoTvName.getText().toString());
        myFriends.setTelephone(autoTvPhone.getText().toString());
//        myFriends.setRemarks(edtSummary.getText().toString());
        myFriends.setSex(showSex);
        myFriends.setRelation(autoTvRelationShip.getText().toString());
        myFriends.setRemindType(tvRemindType.getText().toString());
    }

    /**
     * 底部弹框选择提醒时间
     */
    private void showBottomSheetDialog() {

        final BottomSheetDialog sheetDialog = new BottomSheetDialog(this);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.reminder_settings, null);
        CheckBox day0 = (CheckBox) dialogView.findViewById(R.id.day0);
        CheckBox day1 = (CheckBox) dialogView.findViewById(R.id.day1);
        CheckBox day3 = (CheckBox) dialogView.findViewById(R.id.day3);
        CheckBox day7 = (CheckBox) dialogView.findViewById(R.id.day7);
        CheckBox day15 = (CheckBox) dialogView.findViewById(R.id.day15);
        CheckBox day30 = (CheckBox) dialogView.findViewById(R.id.day30);
        Button buttonCancel = (Button) dialogView.findViewById(R.id.btnCancel);
        Button buttonSure = (Button) dialogView.findViewById(R.id.btnSure);

        //遍历获取选择了哪些checkbox
        final List<CheckBox> checkBoxList = new ArrayList<CheckBox>();
        checkBoxList.add(day0);
        checkBoxList.add(day1);
        checkBoxList.add(day3);
        checkBoxList.add(day7);
        checkBoxList.add(day15);
        checkBoxList.add(day30);

        buttonSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer stringBuffer = new StringBuffer();
                for (CheckBox checkBox : checkBoxList) {
                    if (checkBox.isChecked()) {
                        stringBuffer.append(checkBox.getText().toString()+" ");
                    }
                }
                remindDate = stringBuffer.toString();
                tvRemindDate.setText(remindDate);
                myFriends.setRemindDate(remindDate);
                sheetDialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetDialog.dismiss();
            }
        });
        sheetDialog.setContentView(dialogView);
        sheetDialog.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.type, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.shock:
                break;
            case R.id.alarm:

                break;
            default:
                break;
        }
        tvRemindType.setText(item.getTitle());
        return super.onContextItemSelected(item);
    }
}
