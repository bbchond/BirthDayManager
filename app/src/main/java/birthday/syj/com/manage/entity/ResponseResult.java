package birthday.syj.com.manage.entity;

import birthday.syj.com.manage.util.JsonToString;

/**
 * Created by syj on 2017/12/18.
 */

public class ResponseResult extends JsonToString {

    private String astroname;
    private ResultYear year;
    private ResultWeek week;
    private ResultToday today;
    private ResultTomorrow tomorrow;
    private ResultMonth month;

    public String getAstroname() {
        return astroname;
    }

    public void setAstroname(String astroname) {
        this.astroname = astroname;
    }

    public ResultYear getYear() {
        return year;
    }

    public void setYear(ResultYear year) {
        this.year = year;
    }

    public ResultWeek getWeek() {
        return week;
    }

    public void setWeek(ResultWeek week) {
        this.week = week;
    }

    public ResultToday getToday() {
        return today;
    }

    public void setToday(ResultToday today) {
        this.today = today;
    }

    public ResultTomorrow getTomorrow() {
        return tomorrow;
    }

    public void setTomorrow(ResultTomorrow tomorrow) {
        this.tomorrow = tomorrow;
    }

    public ResultMonth getMonth() {
        return month;
    }

    public void setMonth(ResultMonth month) {
        this.month = month;
    }
}
