package birthday.syj.com.manage.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import birthday.syj.com.manage.DB.MyFriends;
import birthday.syj.com.manage.R;


public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private List<MyFriends> list;
    private Context mContext;
    String daysDivider;
    private int friendsId;

    public FriendsAdapter(List<MyFriends> list) {
        this.list = list;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView getName;
        TextView getDate;
        TextView getBirthDivider;

        public ViewHolder(View itemView) {
            super(itemView);
            getName = (TextView) itemView.findViewById(R.id.name);
            getDate = (TextView) itemView.findViewById(R.id.date);
            getBirthDivider = (TextView) itemView.findViewById(R.id.birthDivider);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        //点击时间
        viewHolder.getName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int position = viewHolder.getAdapterPosition();
//                MyFriends friends = list.get(position);
//                Intent intent = new Intent(mContext, AddBirthDay.class);
//                intent.putExtra("ActivityName", NAME);
//                intent.putExtra("Content", friends);
//                mContext.startActivity(intent);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MyFriends myFriends = list.get(position);
        holder.getDate.setText(myFriends.getDate());
        holder.getName.setText(myFriends.getName());
        holder.itemView.setBackgroundResource(R.drawable.recycler_bg);
        getBirthday(myFriends.getBirthYear()+"-"+myFriends.getBirthMonth()+"-"+myFriends.getBirthDay());
        holder.getBirthDivider.setText(daysDivider+"天"+"");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * 获取两个日期之间的间隔天数
     * @return
     */
    public void getBirthday(String getBirthDate) {
        String birthday = getBirthDate;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        int yearNow = cal.get(Calendar.YEAR);// 获得当前年份
        try {
            Log.i("Birthday_time", formatter.parse(birthday) + "");
            cal.setTime(formatter.parse(birthday));
        } catch (Exception e) {
            e.printStackTrace();
        }
        int birthyear = cal.get(Calendar.YEAR);
        Log.i("Birthday_year", birthyear + "");
        while (birthyear < yearNow) {
            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
            birthyear = cal.get(Calendar.YEAR);
            Log.i("Birthday_year++", birthyear + "");
        }
        Date ed = new Date();
        Log.i("Date_ed", ed + "");
        Date sd = cal.getTime();
        Log.i("Date_sd", ed + "");
        String days;
        if ((ed.getTime() - sd.getTime()) / (3600 * 24 * 1000) < 0) {//>
            days = String.valueOf(-((ed.getTime() - sd.getTime()) / (3600 * 24 * 1000)) + 1);
            daysDivider = days;
        } else {
            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
            sd = cal.getTime();
            days = String.valueOf(-((ed.getTime() - sd.getTime()) / (3600 * 24 * 1000)) + 1);
            daysDivider = days;
        }
    }

}
