package birthday.syj.com.manage.common.showStarInfo;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import birthday.syj.com.manage.R;

public class ShowWeekInfo extends AppCompatActivity {

    TextView weekDate;
    TextView weekLove;
    TextView weekMoney;
    TextView weekCareer;
    TextView weekJob;
    TextView weekHealth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_week_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //添加toolbar支持
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initViews();
        Bundle bundle = getIntent().getExtras();
        weekDate.setText(bundle.getString("getWeekDate"));
        weekLove.setText(bundle.getString("getWeekLove"));
        weekCareer.setText(bundle.getString("getWeekLove"));
        weekJob.setText(bundle.getString("getWeekJob"));
        weekMoney.setText(bundle.getString("getWeekMoney"));
        weekHealth.setText(bundle.getString("getWeekHealth"));
    }

    private void initViews() {
        weekDate= (TextView) findViewById(R.id.date);
        weekJob = (TextView) findViewById(R.id.job);
        weekLove= (TextView) findViewById(R.id.love);
        weekMoney = (TextView) findViewById(R.id.money);
        weekCareer = (TextView) findViewById(R.id.career);
        weekHealth = (TextView) findViewById(R.id.health);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //设置返回标题栏返回键点击事件
            case android.R.id.home:
                finish();
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
