package birthday.syj.com.manage;


import com.facebook.stetho.Stetho;

import org.litepal.LitePalApplication;

/**
 * Created by syj on 2017/12/18.
 */

public class BirthReminderApplication extends LitePalApplication {
    //创建了Application之后要在清单文件内添加name
    private static BirthReminderApplication birthReminderApplication;

    public static BirthReminderApplication getInstance() {
        return birthReminderApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .build());
        birthReminderApplication = this;
    }
}
